import { Component, OnInit } from '@angular/core';
import {DomSanitizer } from '@angular/platform-browser'
import {CarouselService} from '../carousel.service'
import { ActivatedRoute } from '@angular/router';
import { Carousel } from '../carousel';
import { HotelService } from '../hotel.service';
import {  FormBuilder, Validators } from '@angular/forms';
import { TarifHotel } from '../tarif-hotel';
import { TarifHotelService } from '../tarif-hotel.service';
import {  DispoHotel } from '../dispo-hotel';
import { DispoHotelService } from '../dispo-hotel.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Hotel } from '../hotel';

@Component({
  selector: 'app-hoteldetail',
  templateUrl: './hoteldetail.component.html',
  styleUrls: ['./hoteldetail.component.css']
})
export class HoteldetailComponent implements OnInit {
  public dateFormat: String = "yyyy-MM-dd";
  public month: number = new Date().getMonth();
  public fullYear: number = new Date().getFullYear();
  public day: number = new Date().getDay();

  public start: Date = new Date ("2019-09-17"); 
  public end: Date = new Date ("2019-09-18");


  id : any;
  tarifhotel: any;
  disponibility: any;
  nbr_adulte:any;
    nbr_enfant:any;
    hotels:any;
    hotel= new Hotel;
  constructor(private disposerv : DispoHotelService,private tarifserv: TarifHotelService,  private route: ActivatedRoute, private sanitizer : DomSanitizer, private carouselServi : CarouselService, private hotelser : HotelService,private  Form :  FormBuilder) { }
 
  imgcarousels= new Carousel;
   tarif= new TarifHotel;
   dispo= new DispoHotel;
    
   
   
    reservform = this.Form.group({
    nom : [''],
    email :  [''],
    guest :  [''],
    children :  ['']

  });

  ngOnInit() {
   this.id = this.route.snapshot.paramMap.get('id');
   this.imgcarousels.id = this.id;
   console.log(this.imgcarousels)
    this.hetcarousel();
    this.hethotel();
    this.tarif.id=this.id;
     this.dispo.id=this.id;
    }
    
   
hetcarousel(){
   this.carouselServi.getcarousel(this.imgcarousels)
  .subscribe(
    res => {
      console.log(res);
      this.imgcarousels = res;
     
    },
    err => {
      console.log ("erreur");
    }
  )
}
public getSanitizedUrl(img:string) {
        return this.sanitizer.bypassSecurityTrustUrl(img);
    
  
    }
    hethotel(){
      this.hotelser.getHotel()
      
      .subscribe(
        res => {
          this.hotels=res;
          console.log(res);
        for (let hot of this.hotels){
          if (hot.id == this.id){
            this.hotel=hot;
           console.log(this.hotel)

            
          }
        } 
    
        },
        err => {
         console.log("erreur");
        }
      )
  
  }
  onSubmit(){
    console.log(this.reservform.value);
  }
          
   getdate(value:Date){
    this.tarif.check_in=value["0"];
    this.tarif.check_out=value["1"];
    this.dispo.in=value["0"];
    this.dispo.out=value["1"];
    console.log(this.tarif);
    console.log(this.dispo);

    this.hettarif()
    this.hetdispo()
   }

hetdispo(){
  console.log(this.dispo);
this.disposerv.dispohotel(this.dispo)
.subscribe(
  res => {
    console.log(res);
   this.disponibility=res;
  },
  err => {
    console.log ("erreur");
  }
)
 }

    hettarif() {
      console.log(this.tarif);

      this.tarifserv.gettarifhotel(this.tarif)
      .subscribe(
        res => {
          console.log(res);
         this.tarifhotel=res;
        },
        err => {
          console.log ("erreur");
        }
      )
       }
 
       
       getadulte(value:string){
        console.log(value);
       this.nbr_adulte=value;
                              }
       getenfant(value:string){
        console.log(value);
       this.nbr_enfant=value;
                              }




}

