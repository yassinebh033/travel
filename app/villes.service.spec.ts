import { TestBed } from '@angular/core/testing';

import { VillesService } from './villes.service';

describe('VillesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VillesService = TestBed.get(VillesService);
    expect(service).toBeTruthy();
  });
});
