import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HotelsComponent } from './hotels/hotels.component';
import { VoyagesComponent } from './voyages/voyages.component';
import { ContactComponent } from './contact/contact.component';
import {HoteldetailComponent} from './hoteldetail/hoteldetail.component'
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import {GuardGuard} from './guard.guard';

const routes: Routes = [
{path : 'home', component: HomeComponent},
{path: 'hotels', component: HotelsComponent},
{path: 'hotels/:id' , component: HoteldetailComponent},
{ path: 'voyages', component: VoyagesComponent },
{ path : 'contact', component: ContactComponent},
{path:'login', component:LoginComponent},
{path:'admin', component:AdminComponent, canActivate:[GuardGuard]},
{path: '**', component: HomeComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
