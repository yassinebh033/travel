import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Carousel } from './carousel';




const httpOptions={ headers : new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl='https://www.freedomtravel.tn/ng/carouselHotel.php';



@Injectable({
  providedIn: 'root'
})
export class CarouselService {
carousel=new Carousel;
  constructor(private http: HttpClient, private handler : HttpBackend) { 
    this.http=new HttpClient(handler);
   }
  getcarousel(carousel: Carousel): Observable<Carousel>{
    console.log(this.carousel);
    return this.http.post<Carousel>(apiUrl,carousel,httpOptions) ;
    
  }
  
}
