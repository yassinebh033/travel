import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import {Client} from './client'
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class AuthclientService {

  constructor(private http: HttpClient, private _router: Router, private handler : HttpBackend) { 
    this.http=new HttpClient(handler); }

    httpOptions={ headers : new HttpHeaders({'Content-Type' : 'application/json'})};
    apiUrl="http://myfreedobt.cluster011.ovh.net/api/client_indivs.json";

    getClients(): Observable<Client>{
      return this.http.get<Client>(this.apiUrl , this.httpOptions) ;
    }
    getToken(){
      return localStorage.getItem('token');
    }
loggedIn(){
  return !! localStorage.getItem('token')
}
logOut(){
   localStorage.removeItem('token')
this._router.navigate([''])
}


  }


