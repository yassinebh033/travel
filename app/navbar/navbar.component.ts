import { Component, OnInit } from '@angular/core';
import { AuthclientService } from '../authclient.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private auth : AuthclientService) { }

  ngOnInit() {
  }
loggout(){
  return this.auth.logOut();
  console.log("logout");
  
}
logged(){
  return this.auth.loggedIn();
  console.log("login");
}

}
