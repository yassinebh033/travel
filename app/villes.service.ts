import { Injectable } from '@angular/core';
import {HttpClient,HttpBackend} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Villes} from './villes'
@Injectable({
  providedIn: 'root'
})
export class VillesService {
  url='https://www.freedomtravel.tn/ng/villes.php';

  constructor( private http: HttpClient, private handler : HttpBackend) { 
 this.http=new HttpClient(handler);
}

getVilles(): Observable<Villes[]>{
    return this.http.get<Villes[]>(this.url)
    
  }

}