import { Component, OnInit } from '@angular/core';
import { VillesService } from '../villes.service';
import { HotelService } from '../hotel.service';
import {DomSanitizer , SafeResourceUrl} from '@angular/platform-browser'
import {NewsletterService} from '../newsletter.service'
import { Addnewsletter } from '../addnewsletter';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  hotel: any;
  constructor(private villeser : VillesService, private hotelser : HotelService, private sanitizer : DomSanitizer, private news : NewsletterService) { }
  villes : any;
  hotels:any;
  ngOnInit() {
    this.hetelbolden();

    this.hethotel();
    this.addnewss();
  }
  hetelbolden(){
    this.villeser.getVilles()
    .subscribe(
      res => {
        this.villes = res;
        console.log(res);
      },
      err => {
        console.log ("erreur");
      }
    )
  }
  
  hethotel(){
    this.hotelser.getHotel()
    .subscribe(
      res => {
        this.hotels=res;
        console.log(res);
      },
      err => {
        console.log ("erreur");
      }
    )
  }
  
    newsletter = new Addnewsletter;
    addnewss(){
      console.log(this.newsletter);
       this.news.add(this.newsletter)
      .subscribe(
        res => {
          console.log(res);
         let placeholder="your email added! thank you";
        },
        err => {
          console.log ("erreur");
        }
      )
        }

}