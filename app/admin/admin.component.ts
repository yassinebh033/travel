import { Component, OnInit } from '@angular/core';
import{ AuthclientService } from '../authclient.service'



export interface StateGroup {
  letter: string;
  names: string[];
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {
  clients :any;

  constructor(private authc :AuthclientService) { }
  ngOnInit() {
    this.getClient();
  }
getClient(){
  this.authc.getClients()
  .subscribe(
    res => {
      this.clients = res;
      console.log(res);
    },
    err => {
      console.log ("erreur");
    }
  )
}
}
