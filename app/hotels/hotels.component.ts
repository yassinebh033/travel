import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';
import { HttpErrorResponse } from '@angular/common/http';
import { VillesService } from '../villes.service';
import { Rechdispohotel } from '../rechdispohotel';
import { RechdispohotelService } from '../rechdispohotel.service';
@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  public dateFormat: String = "yyyy-MM-dd";
  public month: number = new Date().getMonth();
  public fullYear: number = new Date().getFullYear();
  public day: number = new Date().getDay();

  public start: Date = new Date ("2019-09-17"); 
  public end: Date = new Date ("2019-09-18");
recherche: any;

  url:string;
  hotel: any;
  constructor(private hotelser : HotelService,private villeser : VillesService, private disposerv : RechdispohotelService) { }
  hotels:any;
  villes : any;
  rechdispoHotel= new Rechdispohotel;
  ngOnInit() {
    this.hetelbolden();
    this.hethotel();
  } 


  hethotel(){
    this.hotelser.getHotel()
    
    .subscribe(
      res => {
        this.hotels=res;
        console.log(res);
        
      },
      err => {
        console.log(err);
              
      }
    )

}


hetelbolden(){
  this.villeser.getVilles()
  .subscribe(
    res => {
      this.villes = res;
      console.log(res);
    },
    err => {
      console.log ("erreur");
    }
  )
}


getdestination(value:string){
  console.log(value);
 this.rechdispoHotel.destination_city=value;
                        }

 getdate(value:Date){
  this.rechdispoHotel.check_in=value["0"];
 this.rechdispoHotel.check_out=value["1"];
  console.log(this.rechdispoHotel);
 }

rechercher(){

  console.log(this.rechdispoHotel)
return this.disposerv.recherdispohotel(this.rechdispoHotel)
  .subscribe(
    res => {
      this.recherche = res;
      console.log(res);
    },
    err => {
      console.log ("erreur");
    }
  )
}


}








