import { Injectable } from '@angular/core';
import {HttpClient, HttpBackend} from '@angular/common/http'
import { Hotel } from './hotel';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient, private handler : HttpBackend) { 
    this.http=new HttpClient(handler);}
 url= 'https://www.freedomtravel.tn/ng/hotels.php';
 getHotel(): Observable<Hotel[]>{
  return this.http.get<Hotel[]>(this.url)
  
}
}
