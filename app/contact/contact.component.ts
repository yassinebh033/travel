import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.service';
import {  FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
     

  constructor(private  Form :  FormBuilder , private conta: ContactService) { }
  contactform = this.Form.group({
    nom : ['', Validators.required],
    email :  [''],
    tel :  [''],
    sujet : [''],
    message :  ['']
  });

  ngOnInit() {
    
  }
 

 
  onSubmit(){
    
 this.conta.postcontact(this.contactform.value)
 .subscribe(
  res => {
   console.log(res);
  },
  err => {
    console.log ("erreur");
  }
)

  }
}

