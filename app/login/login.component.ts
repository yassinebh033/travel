import { Component, OnInit } from '@angular/core';
import{AuthentificationService} from '../authentification.service'
import {User} from '../user'
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Router} from '@angular/router'
 @Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth :AuthentificationService, private router:Router) { }
  ngOnInit() {
    
 // this.loginUser();
}
user= new User;


loginUser(){
  this.auth.login(this.user)
  .subscribe(
    res => {
      console.log(res);
      localStorage.setItem('token', res.token);
      localStorage.setItem('refresh_token', res.refresh_token);
      this.router.navigate(['/admin']);
    },
    err => {
      console.log ("erreur");
    }
  )
}

}