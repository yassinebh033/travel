import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import {User} from './user'


const httpOptions={ headers : new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl="http://myfreedobt.cluster011.ovh.net/api/login_check";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(private http: HttpClient, private handler : HttpBackend) { 
    this.http=new HttpClient(handler); }

  login(user : User ): Observable<User>{
    return this.http.post<User>(apiUrl,user,httpOptions) ;
  }
}
