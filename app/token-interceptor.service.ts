import { Injectable } from '@angular/core';
import {HttpRequest,HttpInterceptor, HttpEvent, HttpHandler} from '@angular/common/http'
import {Observable} from 'rxjs'
import {AuthclientService} from './authclient.service'
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private auth: AuthclientService) { }
  intercept(req: HttpRequest<any>, next:HttpHandler): Observable <HttpEvent <any>>{
 req = req.clone({ setHeaders : { 'Authorization' : `Bearer ${this.auth.getToken()}` }});
 return next.handle(req);
}
}